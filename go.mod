module users-microservice

go 1.16

replace gitlab.com/cyverse/cacao-common => /opt/cacao-common
replace gitlab.com/cyverse/cacao-common/service => /opt/cacao-common/service

require (
	github.com/cloudevents/sdk-go v2.4.0+incompatible
	github.com/cloudevents/sdk-go/v2 v2.4.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nats-io/nats.go v1.10.0
	github.com/nats-io/nkeys v0.1.4 // indirect
	github.com/nats-io/stan.go v0.8.2
	github.com/oklog/ulid v1.3.1
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.6.1 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	gitlab.com/cyverse/cacao-common v0.0.0-20210517231454-6861d0be76df
	go.mongodb.org/mongo-driver v1.5.2
)

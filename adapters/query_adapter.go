package adapters

import (
	"context"
	"encoding/json"
	"errors"
	"sync"
	"users-microservice/constants"
	"users-microservice/types"
	"users-microservice/utils"

	"gitlab.com/cyverse/cacao-common/messaging"

	log "github.com/sirupsen/logrus"

	nats "github.com/nats-io/nats.go"

	"gitlab.com/cyverse/cacao-common/service"
)

// QueryAdapter is the adapter implementation for the IncomingQueryPort and OutgoingQueryPort
type QueryAdapter struct {
	natsURL               string
	natsQgroup            string
	domainUserChannel     chan types.UserOp
	domainUserListChannel chan types.UserListQuery
}

// Init - connect to NATS and subscribe to channels
// TODO setup nats attributes as configurable
func (q *QueryAdapter) Init(s constants.Specification) error {
	log.Trace("QueryAdapter.Init()")

	q.natsURL = s.NatsURL
	q.natsQgroup = constants.DefaultNatsQGroup

	return nil
}

// InitChannel initializes the channel between the adapter and domain object
func (q *QueryAdapter) InitChannel(uc chan types.UserOp, ulc chan types.UserListQuery) {
	log.Trace("QueryAdapter.InitChannel()")
	q.domainUserChannel = uc
	q.domainUserListChannel = ulc
}

// Start tells the adapter to start listening for updates. In this case the NATS subscriber would
// listen to a message and then call doAction whenever a message is received.
func (q *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) {
	log.Trace("QueryAdapter.Start()")

	defer wg.Done()

	nc, err := nats.Connect(q.natsURL, nats.MaxReconnects(constants.DefaultNatsMaxReconnect), nats.ReconnectWait(constants.DefaultNatsReconnectWait))
	if err != nil {
		log.Fatal("Cannot connect to nats, ", q.natsURL,
			"(max reconnect = ", constants.DefaultNatsMaxReconnect,
			", reconnect wait = ", constants.DefaultNatsReconnectWait, "): ", err)
	}
	defer nc.Close()
	log.Debug("QueryAdapter.Start: connected to nats")

	// Subscribe
	log.Debug("QueryAdapter.Start: subscribing on subject '", service.NatsSubjectUsers+".*", "'")
	sub, err := nc.QueueSubscribeSync(service.NatsSubjectUsers+".*", q.natsQgroup)
	if err != nil {
		log.Fatal("?QueryAdapter.Start: Cannot create a queued sync subscription" + err.Error())
	}

	// loop until no other messages
	for {

		// create a new child context
		log.Trace("QueryAdapater.Start: create a derived context")
		childCtx, cancel := context.WithTimeout(ctx, constants.DefaultNatsCoreTimeout)

		// get next message; if error, then assume it's a break
		log.Trace("QueryAdapater.Start: waiting for next message")
		m, err := sub.NextMsgWithContext(childCtx)
		cancel() // don't forget to cancel
		if errors.Is(err, context.Canceled) {
			log.Trace("QueryAdapater.Start: context canceled")
			break
		} else if m == nil || err != nil { // timeout
			break
		}

		log.Trace("QueryAdapater.Start: received message on subject = ", m.Subject, ", reply = ", m.Reply)

		// based on the subject do something... or skip
		switch {
		case m.Subject == service.NatsSubjectUsersGet:
			q.processUserGetQuery(m)
		case m.Subject == service.NatsSubjectUsersList:
			q.processUserListQuery(m)
		default:
			log.Warn("Unknown operation was provided, skipping")
		}
	}
	log.Debug("no longer waiting for messages")
}

func (q *QueryAdapter) processUserGetQuery(m *nats.Msg) {
	log.Trace("QueryAdapter.processUserGetQuery() start")

	// get the cloudevent
	ce, err := messaging.ConvertNats(m)
	if err != nil {
		log.Debug("QueryAdapter.processUserGetQuery: error parsing cloudevent from nats: " + err.Error())

		// at this point, there was an error an no User object. Just create a new one and populate the error
		newuser := service.UserModel{Session: service.Session{ErrorType: service.GeneralUnmarshalFromUserMSError, ErrorMessage: err.Error()}}
		q.Respond(m, m.Subject+"Reply", newuser)
		return
	}

	// setup and unmarshal the User query object in question
	uquery := types.UserOp{}
	queryUser := service.UserModel{}
	err = json.Unmarshal(ce.Data(), &queryUser)
	if err != nil {
		log.Debug("QueryAdapter.processUserGetQuery: unmarshaling User object from cloudevent: " + err.Error())

		// at this point, there was an error an no User object. Just create a new one and populate the error
		newuser := service.UserModel{Session: service.Session{ErrorType: service.GeneralUnmarshalFromUserMSError, ErrorMessage: err.Error()}}
		q.Respond(m, m.Subject+"Reply", newuser)
		return
	}
	uquery.User = types.NewDomainUserModel(queryUser)
	uquery.Op = m.Subject
	uquery.ReplyChan = make(chan types.DomainUserModel)

	log.Trace("QueryAdapter.processUserGetQuery: sending domain data")
	q.domainUserChannel <- uquery

	// TODO: EJS, need to add a switch statement and detetct context cancellation
	resp := <-uquery.ReplyChan

	// once we have a response, we need to package everything to send back
	q.Respond(m, m.Subject+"Reply", resp.ConvertToServiceUserModel(queryUser.SessionActor, queryUser.SessionEmulator))
}

func (q *QueryAdapter) processUserListQuery(m *nats.Msg) {
	log.Trace("QueryAdapter.processUserListQuery() start")

	// get the cloudevent
	ce, err := messaging.ConvertNats(m)
	if err != nil {
		log.Debug("QueryAdapter.processUserListQuery: error parsing cloudevent from nats: " + err.Error())

		// at this point, there was an error an no User object. Just create a new one and populate the error
		newlist := q.getInitializedUserList(&service.UserListModel{}, service.GeneralUnmarshalFromUserMSError, "")
		q.Respond(m, m.Subject+"Reply", newlist)
		return
	}

	// setup and unmarshal the User query object in question
	listquery := types.UserListQuery{}
	err = json.Unmarshal(ce.Data(), listquery.UserList)
	if err != nil {
		log.Debug("QueryAdapter.processUserListQuery: unmarshaling User object from cloudevent: " + err.Error())

		// at this point, there was an error an no UserList object. Just create a new one and populate the error
		newlist := q.getInitializedUserList(&service.UserListModel{}, service.GeneralUnmarshalFromUserMSError, "")
		q.Respond(m, m.Subject+"Reply", newlist)
		return
	}
	listquery.Op = m.Subject
	listquery.ReplyChan = make(chan []types.DomainUserModel)

	log.Trace("QueryAdapter.processUserListQuery: sending domain data")
	q.domainUserListChannel <- listquery

	// this will be a []DomainUserModel
	resp := <-listquery.ReplyChan

	// let's take the original UserListModel and update it
	// if there's an error, it will have the error type and message populated
	updatedULM := listquery.UserList
	if updatedULM.ErrorType == "" {
		log.Trace("QueryAdapter.processUserListQuery: no error detected, converted response to UserListModel")
		updatedULM.Users = make([]service.User, len(resp))
		for i, element := range resp {
			updatedULM.Users[i] = element.ConvertToServiceUserModel(updatedULM.SessionActor, updatedULM.SessionEmulator)
		}
	} else {
		log.Trace("QueryAdapter.processUserListQuery: error received, not responding with an empty list")
	}

	// once we have a response, we need to package everything to send back
	q.Respond(m, m.Subject+"Reply", *updatedULM)
}

// Respond responds the the client over nats
func (q *QueryAdapter) Respond(m *nats.Msg, subject string, obj interface{}) {
	log.Trace("QueryAdapter.Respond() start")

	ce, err := messaging.CreateCloudEvent(obj, subject, utils.GetCloudEventSourceString())
	if err != nil {
		log.Error("QueryAdapter.Respond: ?Could not convert response user to a cloudevent, not good!")
		return
	}

	payload, err := json.Marshal(ce)
	if err != nil {
		log.Error("QueryAdapter.Respond: ?Could not json.Marshal a cloudevent, not good!")
		return //
	}

	m.Respond(payload)
}

// getInitializedUserList will take a previous UserList (previousUL) and return
// a prepared user list for return back to the the requestor
func (q *QueryAdapter) getInitializedUserList(previousUL *service.UserListModel, errType string, errMessage string) *service.UserListModel {
	return &service.UserListModel{
		Session: service.Session{
			SessionActor:    previousUL.SessionActor,
			SessionEmulator: previousUL.SessionEmulator,
			ErrorType:       errType,
			ErrorMessage:    errMessage},
		Filter:     previousUL.Filter,
		StartIndex: -1, // -1 if error, assume error
		TotalSize:  -1, // -1 if error, assume error
		NextStart:  -1} // -1 if error, assume error
}
